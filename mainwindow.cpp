#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->setWindowIcon(QIcon("logo.ico"));
    ui->setupUi(this);

    upgrade_progress = NULL;
    timeout_ms = 50;
    timeout_count = 100;


    GetPort();

    //设置默认值
    ui->PortBox->setCurrentIndex(0);
    ui->BaudBox->setCurrentIndex(0);
    ui->PortBox->setCurrentIndex(1);
    ui->StopBox->setCurrentIndex(0);
    ui->DataBox->setCurrentIndex(3);
    ui->ParityBox->setCurrentIndex(0);

    ui->sendButton->setEnabled(false); //发送按钮
    ui->sHexRadio->setEnabled(false);
    ui->sTextRadio->setChecked(true);
    ui->sTextRadio->setEnabled(false);

    ui->rHexRadio->setChecked(true);
    ui->reDisplay->setChecked(true); //显示接收
    ui->wordwrap_checkBox->setChecked(true); //自动换行

    ui->upgradeButton->setEnabled(false);

    ui->openButton->setText("打开串口");

    connect(ui->reSendCheck, &QCheckBox::stateChanged,
            this, &MainWindow::timeTosend);         //自动重发

    time = new QTimer(this);
    //定时发送
    connect(time, &QTimer::timeout, this, &MainWindow::on_sendButton_clicked);//

    //创建线程
    serialThread = new SerialThread;
    mythread = new QThread(this);
    serialThread->moveToThread(mythread);

    //打开串口信号
    connect(this, &MainWindow::init_serial_port, serialThread, &SerialThread::init_port);

    //收到数据显示
    connect(serialThread, SIGNAL(show_receive_data()), this, SLOT(show_receive()), Qt::QueuedConnection);

    //响应串口打开状态
    connect(serialThread, &SerialThread::open_serial_flag, this, &MainWindow::open_serial_handle, Qt::QueuedConnection);

    //升级信号，绑定
    connect(this, &MainWindow::upgrad_start_sig, serialThread, &SerialThread::upgrade_start_slot);

    //显示升级状态
    connect(serialThread, &SerialThread::upgrade_status_sig, this, &MainWindow::display_upgrade_status);

    //界面写串口数据
    connect(this, &MainWindow::write_serial_sig, serialThread, &SerialThread::write_serial);

    //
    connect(this, &MainWindow::upgrade_quit_sig, serialThread, &SerialThread::upgrade_status_quit);

    m_usb_listener = new usb_listener;
    qApp->installNativeEventFilter(m_usb_listener);
    connect(m_usb_listener, &usb_listener::device_plugin, this, &MainWindow::usb_listener_plugin);  //usb插入
    connect(m_usb_listener, &usb_listener::device_plugout, this, &MainWindow::usb_listener_plugout);  //usb插入
}

MainWindow::~MainWindow()
{
    qDebug() << tr("程序退出");
    delete ui;
    delete time;
    mythread->quit();
    mythread->wait();
}



void MainWindow::GetPort()
{
    //获取可用串口
    const auto infos = QSerialPortInfo::availablePorts();
    for(const QSerialPortInfo &info : infos)
    {
        ui->PortBox->addItem(info.portName());
        qDebug() << info.portName();
        #if 0
        QSerialPort serial;
        serial.setPort(info);
        if(serial.open(QIODevice::ReadWrite))
        {
            prot_count ++;
            ui->PortBox->addItem(info.portName());
            qDebug() << info.portName();
            serial.close();
        }
        #endif
    }

}


//定时发送
void MainWindow::timeTosend()
{
    if(ui->reSendCheck->isChecked())
    {
        if(time->isActive())
        {
            return;
        }
        else
        {
            int ms = ui->spinBox->value();
            time->start(ms);
        }
    }
    else
    {
        if(time->isActive())
        {
            time->stop();
        }
        else
        {
            return;
        }
    }
}

QSerialPort::BaudRate MainWindow::get_BaudRate()
{
    switch (ui->BaudBox->currentIndex()) {
    case 0:
        return QSerialPort::Baud9600;
    case 1:
        return QSerialPort::Baud19200;
    case 2:
        return QSerialPort::Baud38400;
    case 3:
        return QSerialPort::Baud57600;
    case 4:
        return QSerialPort::Baud115200;
    default:
        return QSerialPort::Baud9600;
    }

    return QSerialPort::UnknownBaud;
}

QSerialPort::StopBits MainWindow::get_StopBits()
{

    switch (ui->StopBox->currentIndex()) {
    case 0:
        return QSerialPort::OneStop;
    case 1:
        return QSerialPort::OneAndHalfStop;
    case 2:
        return QSerialPort::TwoStop;
    default:
        return QSerialPort::OneStop;
    }

    return QSerialPort::UnknownStopBits;
}

//停止位配置
QSerialPort::DataBits MainWindow::get_DataBits()
{
    switch (ui->DataBox->currentIndex()) {
    case 0:
        return QSerialPort::Data5;
    case 1:
        return QSerialPort::Data6;
    case 2:
        return QSerialPort::Data7;
    case 3:
        return QSerialPort::Data8;
    default:
        return QSerialPort::Data8;
    }

    return QSerialPort::UnknownDataBits;
}

//停止位配置
QSerialPort::Parity MainWindow::get_Parity()
{
    switch (ui->DataBox->currentIndex()) {
    case 0:
        return QSerialPort::NoParity;
    case 1:
        return QSerialPort::EvenParity;
    case 2:
        return QSerialPort::OddParity;
    case 3:
        return QSerialPort::SpaceParity;
    case 4:
        return QSerialPort::MarkParity;
    default:
        return QSerialPort::NoParity;
    }

    return QSerialPort::UnknownParity;
}


//打开按钮
void MainWindow::on_openButton_clicked()
{

    if (ui->openButton->text() == tr("打开串口"))
    {
        serialThread->name = ui->PortBox->currentText();
        serialThread->baudRate = get_BaudRate();
        serialThread->stopBits = get_StopBits();
        serialThread->dataBits = get_DataBits();
        serialThread->parity = get_Parity();

        if(mythread->isRunning() != true)
        {
            qDebug() << "启动子线程";
            //启动线程，但没有启动线程处理函数
            mythread->start();
        }
    }
    emit init_serial_port();//调用 init_port
}

//接收框清除
void MainWindow::on_r_clearButton_clicked()
{
    ui->textBrowser->clear();
}

//发送框清除
void MainWindow::on_s_clearButton_clicked()
{
    ui->lineEdit->clear();
}

//发送按钮
void MainWindow::on_sendButton_clicked()
{
    QString str = ui->lineEdit->text();
    if(!str.isEmpty())
    {
        auto isHexSend = ui->sHexRadio->isChecked();

        int len = str.length();
        if(len%2 == 1)
        {
            str = str.insert(len-1,'0');
        }
        QByteArray senddata;
        if(isHexSend)
        {
            StringToHex(senddata);
            emit write_serial_sig(senddata);
            //serialThread->write_serial(senddata);
            //serialThread->serialPort->write(senddata);
        }
        else
        {
            emit write_serial_sig(ui->lineEdit->text().toLocal8Bit());
            //serialThread->write_serial(ui->lineEdit->text().toLocal8Bit());
            //serialThread->serialPort->write(ui->lineEdit->text().toLocal8Bit());
        }
    }
}

void MainWindow::StringToHex(QByteArray &senddata)
{
    QString hexstr;
    bool ok = false;

    QString send_str = ui->lineEdit->text();

    qDebug() << tr("str = %1").arg(send_str);

    for(int i = 0; i < send_str.length();)
    {
        if(' ' == send_str.at(i))
        {
            i++;
            continue;
        }

        hexstr.clear();
        hexstr += send_str.at(i++);
        hexstr += send_str.at(i++);
        //qDebug() << tr("hexstr = %1").arg(hexstr);
        senddata.append(hexstr.toUInt(&ok, 16));
    }

    return;
#if 0
    int hexdata,lowhexdata;
    int hexdatalen = 0;
    int len = str.length();
    senddata.resize(len/2);
    char lstr,hstr;
    for(int i=0; i<len; )
    {
       //char lstr,
       hstr=str[i].toLatin1();
       if(hstr == ' ')
       {
           i++;
           continue;
       }
       i++;
       if(i >= len)
           break;
       lstr = str[i].toLatin1();
       hexdata = ConvertHexChart(hstr);
       lowhexdata = ConvertHexChart(lstr);
       if((hexdata == 16) || (lowhexdata == 16))
           break;
       else
           hexdata = hexdata*16+lowhexdata;
       i++;
       senddata[hexdatalen] = (char)hexdata;
       hexdatalen++;
    }
    senddata.resize(hexdatalen);
#endif
}

char MainWindow::ConvertHexChart(char ch)
{
    if((ch >= '0') && (ch <= '9'))
                return ch-0x30;  // 0x30 对应 ‘0’
            else if((ch >= 'A') && (ch <= 'F'))
                return ch-'A'+10;
            else if((ch >= 'a') && (ch <= 'f'))
                return ch-'a'+10;
    //        else return (-1);
    else return ch-ch;//不在0-f范围内的会发送成0

}

//选择文件按钮
void MainWindow::on_selectFileButton_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    tr("选择升级文件"),
                                                    "E:",
                                                    tr("图片文件(*.bin);;"
                                                       "图片文件(*.png *.jpg);;"
                                                       "本本文件(*.txt);;"
                                                       "全部(*.*)"));
    ui->filename_lineEdit->setText(filename);
}

//升级按钮
void MainWindow::on_upgradeButton_clicked()
{
    //获取版本信息
    bool ok;
    QString version_str = ui->version_lineEdit->text();

    uint version = version_str.toUInt(&ok, 10);
    if(!ok || version > 255 )
    {
        QMessageBox::critical(this, tr("ERROR"), tr("版本号:1 ~ 255"));
        return;
    }

    serialThread->set_upgrade_version(version);
    serialThread->set_upgrade_filename(ui->filename_lineEdit->text());
    emit upgrad_start_sig();
    ui->upgradeButton->setEnabled(false);

    return;
}

//显示数据
void MainWindow::show_receive()
{
    QByteArray temp = serialThread->receiveData;
    serialThread->receiveData.clear();
    ui->reDisplay->isChecked();

    if(!ui->reDisplay->isChecked()) //不显示
    {
        return;
    }

    if(ui->rHexRadio->isChecked())
    {
        QDataStream out(&temp,QIODevice::ReadOnly);    //将字节数组读入
        while(!out.atEnd())
        {
             qint8 outChar = 0;
             out>>outChar;   //每字节填充一次，直到结束
             //十六进制的转换
             QString str = QString("%1").arg(outChar&0xFF, 2, 16, QLatin1Char('0'));
             ui->textBrowser->insertPlainText(str.toUpper());//大写
             ui->textBrowser->insertPlainText(" ");//每发送两个字符后添加一个空格
             ui->textBrowser->moveCursor(QTextCursor::End);
        }
    }
    else
    {
        QString myStrTemp = QString::fromLocal8Bit(temp);
        ui->textBrowser->append(myStrTemp);
        serialThread->receiveData.clear();
    }
    if(ui->wordwrap_checkBox->isChecked())
    {
        ui->textBrowser->insertPlainText("\n");
    }
    return;
}


void MainWindow::open_serial_handle(int open_flag)
{
    if(open_flag == 1)
    {
        //配置按钮失能
        ui->openButton->setText(tr("关闭串口"));
        ui->PortBox->setEnabled(false);
        ui->BaudBox->setEnabled(false);
        ui->StopBox->setEnabled(false);
        ui->DataBox->setEnabled(false);
        ui->ParityBox->setEnabled(false);

        //使能按钮
        ui->sendButton->setEnabled(true);
        ui->upgradeButton->setEnabled(true);
        ui->sTextRadio->setEnabled(true);
        ui->sHexRadio->setEnabled(true);
        ui->upgradeButton->setEnabled(true);
    }
    else
    {
        if(open_flag < 0)
        {
            QMessageBox::critical(this, tr("错误"), tr("串口打开失败!!!"));
        }

        //配置按钮失能
        ui->openButton->setText(tr("打开串口"));
        ui->PortBox->setEnabled(true);
        ui->BaudBox->setEnabled(true);
        ui->StopBox->setEnabled(true);
        ui->DataBox->setEnabled(true);
        ui->ParityBox->setEnabled(true);

        //使能按钮
        ui->sendButton->setEnabled(false);
        ui->upgradeButton->setEnabled(false);
        ui->sTextRadio->setEnabled(false);
        ui->sHexRadio->setEnabled(false);
        ui->upgradeButton->setEnabled(false);
    }

}

//取消升级
void MainWindow::ProgressDialog_canceled()
{
    qDebug() << __func__;
    if(upgrade_progress != NULL)
    {
        upgrade_progress->deleteLater();
        upgrade_progress = NULL;
    }
    ui->upgradeButton->setEnabled(true);
    emit upgrade_quit_sig(0);
}

//显示升级进度及状态
void MainWindow::display_upgrade_status(int status, QString msg)
{
    //qDebug() << __func__ << msg;

    if(status >= 0)
    {
        //更新进度条
        if(upgrade_progress == NULL)
        {
            upgrade_progress = new QProgressDialog(msg, "取消", 0, 100, this);
            upgrade_progress->setWindowModality(Qt::WindowModal);
            upgrade_progress->setMinimumDuration(2);
            upgrade_progress->setWindowTitle("请稍后");
            upgrade_progress->show();
            connect(upgrade_progress, &QProgressDialog::canceled, this, &MainWindow::ProgressDialog_canceled);
        }

        upgrade_progress->setValue(status);
        if(status >= upgrade_progress->maximum())
        {
            delete upgrade_progress;
            upgrade_progress = NULL;
        }
    }
    else
    {
        ProgressDialog_canceled();
        if(status == -1)
        {
            QMessageBox::information(this, tr("OK"), tr("%1").arg(msg));
        }
        else
        {
            QMessageBox::critical(this, tr("ERROR"), tr("%1").arg(msg));
        }
    }

    qApp->processEvents();
    //qApp->installEventFilter();
    return;
}

//USB插入响应
void MainWindow::usb_listener_plugin()
{
    //刷新端口信息
    QString current_prot = ui->PortBox->currentText();
    ui->PortBox->clear();
    const auto infos = QSerialPortInfo::availablePorts();
    for(const QSerialPortInfo &info : infos)
    {
        ui->PortBox->addItem(info.portName());
        qDebug() << info.portName();
    }
    ui->PortBox->setCurrentText(current_prot);
}

//USB拔出响应
void MainWindow::usb_listener_plugout()
{
    bool flag = false;
    qDebug() << "USB设备拔出";
    QString current_prot = ui->PortBox->currentText();
    const auto infos = QSerialPortInfo::availablePorts();
    ui->PortBox->clear();
    for(const QSerialPortInfo &info : infos)
    {
        ui->PortBox->addItem(info.portName());
        qDebug() << info.portName();
        if(current_prot == info.portName() && serialThread->get_open_flag() == 1)
        {
            flag = true;
        }
    }

    if(flag == false && serialThread->get_open_flag() == 1)
    {
        emit init_serial_port();//调用 init_port 关闭串口
        QMessageBox::warning(this, tr("警告"), tr("打开串口被拔出!!!"));
    }
    else
    {
        ui->PortBox->setCurrentText(current_prot);
    }
}
