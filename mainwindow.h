#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "serialthread.h"
#include "usb_listener.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QMainWindow>
#include <QLabel>
#include <QTimer>
#include <windows.h>
#include <QString>
#include <dbt.h>
#include <QDebug>
#include<devguid.h>
#include<SetupAPI.h>
#include<InitGuid.h>
#include <QThread>
#include <QFile>
#include <QtEndian>
#include <QProgressDialog>



namespace Ui {
class MainWindow;
}

#pragma pack(push)
#pragma pack(1)//单字节对齐(Qt似乎是默认4字节对齐)
typedef struct
{
    quint16 signature;
    quint8 address;
    quint8 cmd;
    quint8 length;
}MSG_HEADER_BLE;
#pragma pack(pop)

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void GetPort();

    ~MainWindow();
    void usb_listener_plugin();
    void usb_listener_plugout();
private slots:
        //void ReadData();//读取数据

        void timeTosend();//定时发送

        void on_openButton_clicked();  //打开串口按钮槽函数

        void on_r_clearButton_clicked();    //接收清除

        void on_s_clearButton_clicked();

        void on_sendButton_clicked();

        void StringToHex(QByteArray & senddata);//用于发送时进制转换

        char ConvertHexChart(char ch);//十六进制转换

        void on_selectFileButton_clicked();

        void on_upgradeButton_clicked(); //升级按钮槽函数


signals:
    void init_serial_port();//启动子线程，将当前的端口名传到
    void write_serial_sig(QByteArray cmd_msg);//前端写数据信号
    void upgrade_quit_sig(int flag);//前端写数据信号
    void upgrad_start_sig();

  //接收数据
public slots:
  void show_receive();

  void open_serial_handle(int open_flag);

  void display_upgrade_status(int status, QString msg);

  void ProgressDialog_canceled();

private:
    Ui::MainWindow *ui;

    QTimer *time;           //用于定时发送

    //串口线程
    SerialThread *serialThread;
    QThread *mythread;
    QProgressDialog *upgrade_progress; //升级进度

    //串口监听
    usb_listener *m_usb_listener;

    quint16 timeout_ms;
    quint8 timeout_count;

    //quint8 prot_count; //扫描到的串口数量

    QSerialPort::BaudRate get_BaudRate();

    QSerialPort::StopBits get_StopBits();
    QSerialPort::DataBits get_DataBits();
    QSerialPort::Parity get_Parity();
};


#endif // MAINWINDOW_H
