#ifndef SERIALTHREAD_H
#define SERIALTHREAD_H

#include <QObject>
#include <QDebug>
#include <QFile>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QTimer>
#include <QDataStream>

class SerialThread : public QObject
{
    Q_OBJECT

public:
    explicit SerialThread(QObject *parent = nullptr);
    ~SerialThread();
    QString name;
    qint32 baudRate;
    QSerialPort::StopBits stopBits;
    QSerialPort::DataBits dataBits;
    QSerialPort::Parity parity;

    QByteArray receiveData;

    void upgrade_end_response(QByteArray data);
    void set_upgrade_version(quint8 version);
    void set_upgrade_filename(QString filename);
    int get_open_flag();
    quint8 rep_msg_calculate_checksum(QByteArray *cmd_msg);
public slots:
  void init_port();
  void read_data();  //处理接收到的数据
  void write_serial(QByteArray cmd_msg);//启动子线程，将当前的端口名传到

  void upgrade_start_slot(); //开始升级，接受升级文件名


  void upgrade_status_quit(int flag); //退出升级状态
  void upgrade_start_response(QByteArray data);  //升级开始响应
  void upgrade_send_file();

  void upgrade_end(); //升级结束
  void upgrade_timeout();
signals:

  //接收数据
  void write_serial_sig(QByteArray cmd_msg);
  void show_receive_data();
  void open_serial_flag(int open_flag);  //发送打开串口的标志
  void upgrade_status_sig(int status, QString msg); //status 0~100升级进度， -1:信息提示 -2:错误提示
private:

    int open_flag;

    //消息头
    quint16 msg_signature;
    quint8 msg_address;
    quint8 msg_cmd;
    quint8 msg_length;

    //升级相关数据
    qint8 upgrade_status;
    quint8 upgrade_version;
    quint16 block_id;   //数据包的流水号
    quint16 max_id;     //当前升级最大流水号
    quint16 block_size;
    quint16 last_block_size;
    quint8 status_failde_count;

    QString upgrade_filename;
    QByteArray upgrade_block; //当前升级的数据帧
    QFile *upgrade_file;        //升级文件
    quint32 file_crc16;

    QTimer *timeout;
    QSerialPort *serialPort;

    QString debug_msg_data(QByteArray *cmd_msg, int flag);
    quint8 msg_calculate_checksum(QByteArray *cmd_msg);


    void upgrade_send_file_response(QByteArray data);
};

#endif // SERIALTHREAD_H
