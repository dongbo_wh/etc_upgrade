#ifndef USB_LISTENER_H
#define USB_LISTENER_H

#include <QWidget>
#include <QAbstractNativeEventFilter>
#include <windows.h>
#include <dbt.h>
class usb_listener : public QWidget, public QAbstractNativeEventFilter
{
    Q_OBJECT
public:
    explicit usb_listener(QWidget *parent = nullptr);

protected:
    bool nativeEventFilter(const QByteArray &eventType, void *message, long *result);

signals:
    void device_changeCbk();
    void device_plugin();
    void device_plugout();
};

#endif // USB_LISTENER_H
