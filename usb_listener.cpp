#include "usb_listener.h"
#include <QApplication>
#include <QDebug>

usb_listener::usb_listener(QWidget *parent) : QWidget(parent)
{

}

bool usb_listener::nativeEventFilter(const QByteArray &eventType, void *message, long *result)
{
    MSG* msg = reinterpret_cast<MSG*>(message);
    unsigned int msg_type = msg->message;
    if(msg_type == WM_DEVICECHANGE)
    {
        emit device_changeCbk();
        if(msg->wParam == DBT_DEVICEARRIVAL)
        {
            emit device_plugin();         //触发信号
        }
        if(msg->wParam == DBT_DEVICEREMOVECOMPLETE)
        {
            emit device_plugout();         //触发信号
        }

    }

    return QWidget::nativeEvent(eventType, message, result);
}
