#include "serialthread.h"
#include <QThread>

static const unsigned short crc16tab[256]= {
    0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
    0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
    0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
    0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
    0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
    0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
    0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
    0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
    0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
    0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
    0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
    0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
    0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
    0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
    0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
    0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
    0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
    0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
    0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
    0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
    0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
    0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
    0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
    0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
    0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
    0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
    0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
    0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
    0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
    0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
    0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
    0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};

//工具函数
QString SerialThread::debug_msg_data(QByteArray *cmd_msg, int flag)
{
    QString str;

    if(flag == 1) //写
    {
        str += "->";
    }
    else
    {
        str += "<-";
    }


    for(int i = 0; i < cmd_msg->size(); i++)
    {
        str += QString("%1").arg((quint8)cmd_msg->at(i), 2, 16, (QChar)'0').toUpper();

        if(i != cmd_msg->size() -1)
        {
            str.append(" ");
        }
    }
    qDebug() << str;
    return str;
}

quint16 crc16_ccitt(quint16 crc, QByteArray *buf)
{
    for(int i = 0; i < buf->size(); i++)
    {
        crc = (crc<<8) ^ crc16tab[( (crc>>8) ^ (buf->at(i)) )& 0x00FF];
    }
    return crc;
}

SerialThread::SerialThread(QObject *parent) : QObject(parent)
{
    open_flag = 0;
    msg_signature = 0xAA55;
    msg_address = 0x10;
    upgrade_version = 0xA;
    upgrade_status = 0;
    block_id = 0;
    last_block_size = 0;
    status_failde_count = 0;

    //serialPort = new QSerialPort;

    connect(this, SIGNAL(write_serial_sig(QByteArray)), this, SLOT(write_serial(QByteArray)));

    //timeout = new QTimer;
    //connect(timeout, &QTimer::timeout, this, &SerialThread::upgrade_timeout);//
}

SerialThread::~SerialThread()
{
    if(serialPort)
    {
        serialPort->deleteLater();
    }
}


void SerialThread::init_port()
{
    if(open_flag != 1)
    {
        qDebug() << "打开串口";

        serialPort = new QSerialPort;
        serialPort->setPortName(name);
        if(serialPort->open(QIODevice::ReadWrite))
        {
            serialPort->setBaudRate(QSerialPort::Baud9600);
            serialPort->setStopBits(QSerialPort::OneStop);
            serialPort->setDataBits(QSerialPort::Data8);
            serialPort->setParity(QSerialPort::NoParity);
            open_flag = 1;
            connect(serialPort, SIGNAL(readyRead()), this, SLOT(read_data()), Qt::QueuedConnection); //Qt::DirectConnection
        }
        else
        {
            qDebug() << tr("串口打开失败");
            open_flag = -1;
            serialPort->deleteLater();
        }

        if(NULL == timeout)
        {
            timeout = new QTimer;
            connect(timeout, &QTimer::timeout, this, &SerialThread::upgrade_timeout);//
        }
    }
    else
    {
        qDebug() << "关闭串口";
        open_flag = 0;
        serialPort->deleteLater();

        if(NULL != timeout)
        {
            timeout->deleteLater();
            timeout = NULL;
        }
    }
    emit open_serial_flag(open_flag);
    return;
}

void SerialThread::read_data()
{
    qDebug() << tr("upgrade_status : %1").arg(upgrade_status);
    qDebug() << __func__;
    QByteArray data = serialPort->readAll();
    debug_msg_data(&data, 0);
    switch (upgrade_status) {
    case 1: //升级开始响应
        upgrade_start_response(data);
        break;

    case 2: //发送数据
        upgrade_send_file_response(data);
        break;

    case 3: //升级结束
        upgrade_end_response(data);
        break;

    default:
        receiveData += data;
        emit show_receive_data();

    }
    //qDebug() << "handing thread is:" << QThread::currentThreadId();
}

void SerialThread::write_serial(QByteArray cmd_msg)
{
    qDebug() << __func__;
    debug_msg_data(&cmd_msg, 1);
    if(serialPort->write(cmd_msg) < 0)
    {
        qDebug() << "serialPort->write" <<serialPort->errorString();

        return;
    }
    //serialPort->clear();
}

quint8 SerialThread::msg_calculate_checksum(QByteArray *cmd_msg)
{
    quint8 checksum = 0;
    for(int i = 2; i < cmd_msg->size(); i++)
    {
        checksum += cmd_msg->at(i);
    }
    return checksum;
}

quint8 SerialThread::rep_msg_calculate_checksum(QByteArray *cmd_msg)
{
    quint8 checksum = 0;
    for(int i = 2; i < cmd_msg->size() - 1; i++)
    {
        checksum += cmd_msg->at(i);
    }
    return checksum;
}

//升级错误处理
void SerialThread::upgrade_status_quit(int flag)
{
    qDebug() << "退出升级状态" <<flag;
    if(upgrade_file != NULL)
    {
        upgrade_file->deleteLater();
        upgrade_file = NULL;
    }
    upgrade_status = flag; //退出升级状态
    block_id = 0;
    max_id = 0;
    upgrade_block.clear();

    if(1 != flag) //非升级开始状态
    {
        status_failde_count = 0;
    }
    if(timeout != NULL)
    {
        timeout->stop();
    }
}

//开始升级
void SerialThread::upgrade_start_slot()
{
    upgrade_file = new QFile(upgrade_filename);

    if(!upgrade_file->open(QIODevice::ReadOnly))
    {
        upgrade_status = 0;
        emit upgrade_status_sig(-2, tr("%1").arg(upgrade_file->errorString()));
        qDebug() << upgrade_file->errorString();
        return;
    }

    if(upgrade_file->size() > (quint32)0xffffffff)
    {
        upgrade_status = -1;
        emit upgrade_status_sig(-2, tr("文件过大"));
        qDebug() << "文件过大";
        return;
    }

    qDebug() << tr("开始升级");
    msg_cmd = 0x1A;  //升级开始
    msg_length = 5; //数据长度

    upgrade_block.clear();
    QDataStream msg_stream(&upgrade_block, QIODevice::WriteOnly);
    msg_stream.setByteOrder(QDataStream::BigEndian);  //大端模式
    msg_stream << msg_signature << msg_address << msg_cmd << msg_length;
    msg_stream << upgrade_version << (quint32)upgrade_file->size();
    msg_stream << msg_calculate_checksum(&upgrade_block);

    //设置状态
    upgrade_status = 1; //开始升级
    file_crc16 = 0;
    write_serial(upgrade_block);
    emit upgrade_status_sig(0, tr("开始升级...."));
    timeout->start(3 * 1000);
    return;
}

//升级开始响应
void SerialThread::upgrade_start_response(QByteArray data)
{
    quint16 rep_signature;
    quint8 rep_address;
    quint8 rep_cmd;
    quint8 rep_length;
    quint8 rc = 0; //响应码
    quint8 checksum = 0;

    QDataStream rece_stream(&data, QIODevice::ReadOnly);
    rece_stream.setByteOrder(QDataStream::BigEndian);  //大端模式
    rece_stream >> rep_signature >> rep_address >> rep_cmd >> rep_length;
    if(msg_signature != rep_signature || rep_address != msg_address || rep_cmd != msg_cmd)
    {
        upgrade_status = -1;
        qDebug() << tr("命令响应格式校验错误!");
        emit upgrade_status_sig(-2, tr("命令响应格式校验错误!"));
        return;
    }
    rece_stream >> rc >> block_size >> checksum;

    if(0 != rc)
    {
        upgrade_status = -1;
        emit upgrade_status_sig(-2, tr("命令响应错误 code:%1 !").arg(rc));
        qDebug() << tr("命令响应错误 code:%1 !").arg(rc);
        return;
    }

    //检查校验和
    if(checksum != rep_msg_calculate_checksum(&data))
    {
        emit upgrade_status_sig(-2, tr("响应消息校验和错误!"));
        qDebug() << tr("响应消息校验和错误!");
    }

    //更新状态
    upgrade_status = 2; //发送数据
    max_id = upgrade_file->size() / block_size;
    last_block_size = upgrade_file->size() % block_size;
    if(last_block_size)
    {
        max_id++;
    }
    block_id = 1;

    //开始发送数据
    upgrade_send_file();
    return;
}

//发送数据
void SerialThread::upgrade_send_file()
{
    if(upgrade_status != 2) //升级状态不对
    {
        upgrade_status = -1;
        emit upgrade_status_sig(-2, tr("状态错误"));
    }

    if(upgrade_file == NULL || !upgrade_file->isOpen())
    {
        upgrade_status = -1;
        emit upgrade_status_sig(-2, tr("文件打开错误"));
    }

    msg_cmd = 0x1B;

    QByteArray buffer = upgrade_file->read(block_size);
    file_crc16 = crc16_ccitt((quint16)file_crc16, &buffer);

    if(last_block_size > 0 && block_id == max_id)  //最后一帧
    {
        if(last_block_size != buffer.size())
        {
            upgrade_status = -1;
            emit upgrade_status_sig(-2, tr("读文件错误！"));
            return;
        }
        msg_length = last_block_size + sizeof(block_id);
    }
    else
    {
        if(block_size != buffer.size())
        {
            upgrade_status = -1;
            emit upgrade_status_sig(-2, tr("读文件错误！"));
            return;
        }
        msg_length = block_size + sizeof(block_id);
    }

    upgrade_block.clear();
    QDataStream msg_stream(&upgrade_block, QIODevice::WriteOnly);
    msg_stream.setByteOrder(QDataStream::BigEndian);  //大端模式
    msg_stream << msg_signature << msg_address << msg_cmd << msg_length << block_id;
    upgrade_block.append(buffer);
    upgrade_block.append(msg_calculate_checksum(&upgrade_block));

    write_serial(upgrade_block);
    timeout->start(5 * 1000);

    return;
}

//发送数据响应
void SerialThread::upgrade_send_file_response(QByteArray data)
{
    quint16 rep_signature;
    quint8 rep_address;
    quint8 rep_cmd;
    quint8 rep_length;
    quint16 rep_id = 0; //响应ID
    quint8 checksum = 0;

    QDataStream rece_stream(&data, QIODevice::ReadOnly);
    rece_stream.setByteOrder(QDataStream::BigEndian);  //大端模式
    rece_stream >> rep_signature >> rep_address >> rep_cmd >> rep_length;
    if(msg_signature != rep_signature || rep_address != msg_address || rep_cmd != msg_cmd)
    {
        upgrade_status = -1;
        emit upgrade_status_sig(-2, tr("命令响应格式校验错误!"));
        return;
    }
    rece_stream >> rep_id >> checksum;
    if(rep_id != block_id)
    {
        upgrade_status = -1;
        emit upgrade_status_sig(-2, tr("升级响应错误!"));
        qDebug() <<  tr("rep_id:%1 block_id:%2").arg(rep_id).arg(block_id);
        return;
    }


    //检查校验和
    if(checksum != rep_msg_calculate_checksum(&data))
    {
        emit upgrade_status_sig(-2, tr("响应消息校验和错误!"));
        qDebug() << tr("响应消息校验和错误!");
    }

    //成功 更新状态
    int progress = 100 * block_id / max_id;

    if(block_id < max_id)
    {
        block_id++;
        emit upgrade_status_sig(progress, tr("升级中..!"));
        upgrade_send_file();//继续发送数据
    }
    else
    {
        upgrade_status = 3; //升级结束
        emit upgrade_status_sig(progress, tr("数据发送完成!"));

        //发送升级结束命令
        upgrade_end();
    }

    return;
}

//发送升级结束命令
void SerialThread::upgrade_end()
{

    msg_cmd = 0x1C;
    msg_length = 4;
    upgrade_block.clear();
    QDataStream msg_stream(&upgrade_block, QIODevice::WriteOnly);
    msg_stream.setByteOrder(QDataStream::BigEndian);  //大端模式
    msg_stream << msg_signature << msg_address << msg_cmd << msg_length << file_crc16;
    upgrade_block.append(msg_calculate_checksum(&upgrade_block));
    write_serial(upgrade_block);

    //更新状态
    upgrade_status = 3;
    timeout->start(5 * 1000); //设置超时

    return;
}

void SerialThread::upgrade_end_response(QByteArray data)
{
    quint16 rep_signature;
    quint8 rep_address;
    quint8 rep_cmd;
    quint8 rep_length;
    quint8 rc = 0; //响应码
    quint8 checksum = 0;

    QDataStream rece_stream(&data, QIODevice::ReadOnly);
    rece_stream.setByteOrder(QDataStream::BigEndian);  //大端模式
    rece_stream >> rep_signature >> rep_address >> rep_cmd >> rep_length;
    if(msg_signature != rep_signature || rep_address != msg_address || rep_cmd != msg_cmd)
    {
        upgrade_status = -1;
        emit upgrade_status_sig(-2, tr("命令响应格式校验错误!"));
        return;
    }
    rece_stream >> rc >> checksum;
    if(rc != 0)
    {
        upgrade_status = -1;
        emit upgrade_status_sig(-2, tr("结束命令响应错误 code:%1 !").arg(rc));
        qDebug() << tr("结束命令响应错误 code:%1 !").arg(rc);
        return;
    }

    //检查校验和
    if(checksum != rep_msg_calculate_checksum(&data))
    {
        emit upgrade_status_sig(-2, tr("响应消息校验和错误!"));
        qDebug() << tr("响应消息校验和错误 checksum = %1 rep_checksum = %2\n!").arg(checksum).arg(rep_msg_calculate_checksum(&data));
        return;
    }

    //更新显示
    timeout->stop();
    status_failde_count = 0;
    emit upgrade_status_sig(-1, tr("升级成功!"));

    return;

}

void SerialThread::set_upgrade_version(quint8 version)
{
    upgrade_version = version;
}

void SerialThread::set_upgrade_filename(QString filename)
{
    upgrade_filename = filename;
}

int SerialThread::get_open_flag()
{
    return open_flag;
}

void SerialThread::upgrade_timeout()
{
    qDebug() << "超时退出";

    if(upgrade_status == 1) //开始升级
    {
        if(status_failde_count < 5)
        {
            status_failde_count++;
            upgrade_status_quit(1);
            upgrade_start_slot();
            return;
        }
    }

    timeout->stop();
    emit upgrade_status_sig(-2, tr("超时退出!"));
}
